package com.wlhlearn;

import java.io.Serializable;

/**
 * @Author: wlh
 * @Date: 2019/6/11 10:08
 * @Version 1.0
 * @despricate:learn
 */
public class User  implements Serializable{

    private  String name ;
    private  Integer age ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
