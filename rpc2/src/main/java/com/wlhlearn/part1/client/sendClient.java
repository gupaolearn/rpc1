package com.wlhlearn.part1.client;

import com.wlhlearn.User;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @Author: wlh
 * @Date: 2019/6/11 10:08
 * @Version 1.0
 * @despricate:learn
 */
public class sendClient {

    public static void main(String[] args) throws IOException {

        Socket socket=null;
        socket=new Socket("localhost",8080);

        User user=new User() ;
        user.setName("mic");
        user.setAge(18);

        ObjectOutputStream outputStream=new ObjectOutputStream(socket.getOutputStream());

        outputStream.writeObject(user);

        outputStream.close();

    }
}
