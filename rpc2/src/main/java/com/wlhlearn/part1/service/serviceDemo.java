package com.wlhlearn.part1.service;

import com.wlhlearn.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author: wlh
 * @Date: 2019/6/11 10:10
 * @Version 1.0
 * @despricate:learn
 */
public class serviceDemo {


    public static void main(String[] args) throws IOException, ClassNotFoundException {

        ServerSocket socket=null ;

        socket=new ServerSocket(8080);

        Socket socket1=socket.accept();

        ObjectInputStream inputStream=new ObjectInputStream(socket1.getInputStream());

        User user=(User)inputStream.readObject();

        inputStream.close();

        System.out.println(user.toString());

    }
}
